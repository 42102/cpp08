
#ifndef SPAN_HPP
#define SPAN_HPP

#include <iostream>
#include <vector>
#include <algorithm>

class MaxStorageException: public std::exception
{
	private:
		std::string _msg;
	
	public:
		MaxStorageException(void);
		virtual ~MaxStorageException() throw();
		virtual const char * what() const throw();	
};

class NotEnoughNumbersException: public std::exception
{
	private:
		std::string _msg;

	public:
		NotEnoughNumbersException(void);
		~NotEnoughNumbersException() throw();
		virtual const char * what() const throw();
};

class Span
{
	private:
		unsigned int _maximum_size;
		unsigned int _size;
		std::vector<int> vec;

	public:
		//Constructors and destructor
		Span(void);
		Span(const Span & span_copy);
		Span(unsigned int maximum_size);
		~Span();

		//Operators
		Span& operator= (const Span& span_copy);

		//Methods of Span class
		void addNumber(int number);
		int longestSpan(void) const;
		int shortestSpan(void) const;
		void introduceRange(std::vector<int>::iterator begin, std::vector<int>::iterator end);
};


#endif
