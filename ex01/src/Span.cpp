
#include "../inc/Span.hpp"

//Constructors and destructor of Span class
Span::Span(void)
	: _maximum_size(5), _size(0), vec()
{
	std::cout<<"Calling Span default constructor"<<std::endl;
}

Span::Span(const Span& span_copy)
{
	std::cout<<"Calling Span copy constructor"<<std::endl;
	*this = span_copy;
}
Span::Span(unsigned int maximum_size)
	:_maximum_size(maximum_size), _size(0), vec()
{
	std::cout<<"Calling Span constructor with 1 parameter <unsigned int>"
		<<std::endl;
}

Span::~Span()
{
	std::cout<<"Calling destructor of Span"<<std::endl;
}

//Operators of Span class

Span& Span::operator= (const Span& span_copy)
{
	if(this != &span_copy)	
	{
		std::vector<int>::iterator init = this->vec.begin();
		std::vector<int>::iterator end = this->vec.end();

		vec.erase(init, end);
		
		this->_maximum_size = span_copy._maximum_size;
		this->_size = span_copy._size;
		
		copy(span_copy.vec.begin(), span_copy.vec.end(), back_inserter(this->vec));
	}

	return *this;
}

//Methods of Span class

void Span::addNumber(int number)
{
	if(this->_size < this->_maximum_size)
	{
		this->_size++;
		this->vec.push_back(number);
	}
	else
		throw MaxStorageException();
		
}

int Span::longestSpan(void) const
{
	int max;
	int min;

	if(this->_size > 1)
	{
		max = *max_element(this->vec.begin(), this->vec.end());
		min = *min_element(this->vec.begin(), this->vec.end());

		return (max - min);
	}
	else 
		throw NotEnoughNumbersException();
}

int Span::shortestSpan(void) const
{
	int min_diff;

	std::vector<int> copy_vec;

	copy(this->vec.begin(), this->vec.end(), back_inserter(copy_vec));
	sort(copy_vec.begin(), copy_vec.end());
	
	min_diff = INT_MAX;	
	for(std::vector<int>::iterator it = copy_vec.begin(); it != copy_vec.end(); it++)
	{
		if((it + 1) != copy_vec.end() && (*(it + 1) - *it) < min_diff)
			min_diff = (*(it + 1) - *it);
	}
	return min_diff;
}

void Span::introduceRange(std::vector<int>::iterator begin, std::vector<int>::iterator end)
{
	this->vec.insert(this->vec.begin(), begin, end);
}
//Class MaxStorageException

MaxStorageException::MaxStorageException(void)
	: _msg("error: Max Storage Exception!")
{}


MaxStorageException::~MaxStorageException() throw()
{}

const char * MaxStorageException::what() const throw()
{
	return (this->_msg.c_str());
}

//Class NotEnoughNumnbersExceptions

NotEnoughNumbersException::NotEnoughNumbersException(void)
	: _msg("error: Not Enough Numbers Exception!")
{}

NotEnoughNumbersException::~NotEnoughNumbersException() throw()
{}

const char * NotEnoughNumbersException::what() const throw()
{
	return (this->_msg.c_str());
}
