
#include <iostream>
#include "../inc/Span.hpp"

int main(void)
{
	Span sp = Span(5);
	std::vector<int> arr;

	arr.push_back(-203);
	arr.push_back(24);
	arr.push_back(-71);
	arr.push_back(34);
	arr.push_back(5);
	
	try
	{	
		sp.addNumber(6);
		sp.addNumber(3);
		sp.addNumber(17);
		sp.addNumber(9);
		sp.addNumber(11);
		sp.introduceRange(arr.begin(), arr.end());
		std::cout << sp.longestSpan() << std::endl;
		std::cout << sp.shortestSpan() << std::endl;
	}
	catch(std::exception &e)
	{
		std::cout<<e.what()<<std::endl;
	}
	return (0);
}
