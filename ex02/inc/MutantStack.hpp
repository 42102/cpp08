
#if !defined(MUTANT_STACK_HPP)
#define MUTANT_STACK_HPP

#include <stack>

template <typename T>
class MutantStack: public std::stack<T>
{
	private:
		
	public:
		//Constructors and destructor
		MutantStack(void);
		MutantStack(const MutantStack& cpy);
		~MutantStack();

		//Operators
		MutantStack& operator= (const MutantStack &cpy);

		typedef typename std::stack<T>::container_type::iterator iterator;

		iterator begin(void);
		iterator end(void);
		
};

#include "../src/MutantStack.tpp"

#endif
