
#include "../inc/MutantStack.hpp"

//Constructors and destructor of MutantStack class

template <typename T>
MutantStack<T>::MutantStack(void)
{
	std::cout<<"Calling MutantStack default constructor"<<std::endl;
}

template <typename T>
MutantStack<T>::MutantStack(const MutantStack<T>& cpy)
{
	*this = cpy;
}

template <typename T>
MutantStack<T>::~MutantStack()
{
	std::cout<<"Calling MutantStack destructor"<<std::endl;	
}

//Operators of MutantStack class

template <typename T>
MutantStack<T>& MutantStack<T>::operator= (const MutantStack<T>& cpy)
{

	if(this != &cpy)
	{
		std::stack<T>::operator=(cpy);
	}

	return *this;
}

//Implement iterator

template <typename T>
typename MutantStack<T>::iterator MutantStack<T>::begin(void)
{
	return this->c.begin();
}

template <typename T>
typename MutantStack<T>::iterator MutantStack<T>::end(void)
{
	return this->c.end();	
}
