
#include <iostream>
#include "../inc/MutantStack.hpp"

int main(void)
{
	/*
	::MutantStack<float> ms;
	ms.push(12.34f);
	ms.push(-212.34f);
	ms.push(894.234f);
	ms.push(-265.324f);
	std::cout<<"top: "<<ms.top()<<std::endl;

	*/

	MutantStack<int> mstack;
	
	mstack.push(5);
	mstack.push(17);
	
	std::cout << mstack.top() << std::endl;
	
	mstack.pop();
	
	std::cout << mstack.size() << std::endl;
	
	mstack.push(3);
	mstack.push(5);
	mstack.push(737);
	//[...]
	mstack.push(0);
	
	MutantStack<int>::iterator it = mstack.begin();
	MutantStack<int>::iterator ite = mstack.end();
	
	++it;
	--it;
	while (it != ite)
	{
	std::cout << *it << std::endl;
	++it;
	}
	std::stack<int> s(mstack);
	return (0);
}
