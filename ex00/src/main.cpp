
#include <iostream>
#include <../inc/easyfind.hpp>
#include <vector>

int main(void)
{
    int numbers[] = {-12, 92, 2, 10, -102, -1, 7}; 
    
    std::vector<int> array_int(numbers, numbers + (sizeof(numbers)/sizeof(numbers[0])));
    std::vector<int>::iterator new_it;
    try
    {
        new_it = ::easy_find(array_int, -102);
        std::cout<<"new_it: "<<*new_it<<std::endl;
    }catch(std::exception &e)
    {
        std::cout<<e.what()<<std::endl;
    }
    
    return (0);
}
