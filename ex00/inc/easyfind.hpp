
#if !defined(EASYFIND_HPP)
#define EASYFIND_HPP
#include <iostream>
#include <algorithm>
#include <iterator>

class NumberNotFoundException: public std::exception
{
    private:
        std::string msg;
    
    public:
        NumberNotFoundException(void)
            : msg("error: Number Not Found")
            {}

        virtual ~NumberNotFoundException() throw (){}
        virtual const char *what() const throw()
        {
            return (this->msg.c_str());
        }
};
#include "easyfind.tpp"
#endif
