
#if !defined(EASYFIND_TPP)
#define EASYFIND_TPP

#include "easyfind.hpp"

template<typename T >
typename T::iterator easy_find(T& vec, int x)
{
    typename T::iterator it;
    
    it = std::find(vec.begin(), vec.end(), x);

    if(it != vec.end())
        return (it);
    else
      throw NumberNotFoundException(); 
    return it;
}

#endif
